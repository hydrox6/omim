package com.mapswithme.maps.settings;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mapswithme.maps.BuildConfig;
import com.mapswithme.maps.Framework;
import com.mapswithme.maps.R;
import com.mapswithme.util.Constants;
import com.mapswithme.util.Graphics;
import com.mapswithme.util.Utils;
import com.mapswithme.util.sharing.ShareOption;
import com.mapswithme.util.statistics.AlohaHelper;
import com.mapswithme.util.statistics.Statistics;

public class AboutFragment extends BaseSettingsFragment
                        implements View.OnClickListener
{
  private void setupItem(@IdRes int id, boolean tint, @NonNull View frame)
  {
    TextView view = frame.findViewById(id);
    view.setOnClickListener(this);
    if (tint)
      Graphics.tint(view);
  }

  @Override
  protected int getLayoutRes()
  {
    return R.layout.about;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
  {
    View root = super.onCreateView(inflater, container, savedInstanceState);

    ((TextView) root.findViewById(R.id.version))
        .setText(getString(R.string.version, BuildConfig.VERSION_NAME));

    ((TextView) root.findViewById(R.id.data_version))
        .setText(getString(R.string.data_version, Framework.nativeGetDataVersion()));

    setupItem(R.id.copyright, false, root);

    return root;
  }

  private void openLink(@NonNull String link)
  {
    Utils.openUrl(getActivity(), link);
  }

  private void onPrivacyPolicyClick()
  {
    openLink(Framework.nativeGetPrivacyPolicyLink());
  }

  private void onTermOfUseClick()
  {
    openLink(Framework.nativeGetTermsOfUseLink());
  }

  @Override
  public void onClick(View v)
  {
    try
    {
      switch (v.getId())
      {
      case R.id.copyright:
        Statistics.INSTANCE.trackEvent(Statistics.EventName.Settings.COPYRIGHT);
        AlohaHelper.logClick(AlohaHelper.Settings.COPYRIGHT);
        getSettingsActivity().replaceFragment(CopyrightFragment.class,
                                              getString(R.string.copyright), null);
        break;
      }
    } catch (ActivityNotFoundException e)
    {
      AlohaHelper.logException(e);
    }
  }
}
