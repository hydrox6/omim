package com.mapswithme.maps.ads;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public abstract class AdDataAdapter<T>
{
  @NonNull
  private final T mAd;

  protected AdDataAdapter(@NonNull T ad)
  {
    mAd = ad;
  }

  @NonNull
  protected T getAd()
  {
    return mAd;
  }

  @Nullable
  public abstract String getTitle();
  @Nullable
  public abstract String getText();
  @Nullable
  public abstract String getIconImageUrl();
  @Nullable
  public abstract String getCallToAction();
  @Nullable
  public abstract String getPrivacyInfoUrl();
  @NonNull
  public abstract NetworkType getType();

}
