package com.mapswithme.maps.analytics;

import android.app.Activity;
import android.app.Application;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mapswithme.maps.BuildConfig;
import com.mapswithme.maps.PrivateVariables;

import java.util.Map;

class FlurryEventLogger extends DefaultEventLogger
{
  FlurryEventLogger(@NonNull Application application)
  {
    super(application);
  }

  @Override
  public void initialize()
  {
  }

  @Override
  public void logEvent(@NonNull String event, @NonNull Map<String, String> params)
  {
    super.logEvent(event, params);
  }

  @Override
  public void startActivity(@NonNull Activity context)
  {
    super.startActivity(context);
  }

  @Override
  public void stopActivity(@NonNull Activity context)
  {
    super.stopActivity(context);
  }
}
