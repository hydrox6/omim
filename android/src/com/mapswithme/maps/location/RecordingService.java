package com.mapswithme.maps.location;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.androidlibrary.widgets.RemoteViewsCompat;
import com.mapswithme.maps.MwmActivity;
import com.mapswithme.maps.MwmApplication;
import com.mapswithme.maps.R;
import com.mapswithme.util.ThemeUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class RecordingService extends Service {
    public static final String TAG = RecordingService.class.getSimpleName();

    public static final int NOTIFICATION_RECORDING_ICON = 1;

    public static final String RECORDING = "recording";

    public static String SHOW_ACTIVITY = RecordingService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = RecordingService.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static String STOP_BUTTON = RecordingService.class.getCanonicalName() + ".STOP_BUTTON";

    RecordingReceiver receiver;
    Handler handler;
    Notification notification;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH.mm.ss");
    Runnable refresh = new Runnable() {
        @Override
        public void run() {
            refresh();
        }
    };

    public class RecordingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                // showRecordingActivity();
            }
            if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                // do nothing. do not annoy user. he will see alarm screen on next screen on event.
            }
        }
    }

    public static boolean isRecording(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long time = shared.getLong(RECORDING, 0);
        return time != 0;
    }

    public static void startIfEnabled(Context context) {
        if (!isRecording(context))
            return;
        startService(context);
        MwmActivity.startHelper(context);
    }

    public static void startService(Context context) {
        if (!isRecording(context)) {
            SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor edit = shared.edit();
            edit.putLong(RECORDING, System.currentTimeMillis());
            edit.commit();
            TrackRecorder.nativeClearTrack();
        }
        context.startService(new Intent(context, RecordingService.class));
    }

    public static void updateService(Context context) {
        context.startService(new Intent(context, RecordingService.class));
    }

    public static void stopService(Context context) {
        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        long time = shared.getLong(RECORDING, 0);
        SharedPreferences.Editor edit = shared.edit();
        edit.putLong(RECORDING, 0);
        edit.commit();
        context.stopService(new Intent(context, RecordingService.class));
        MwmActivity.stopRecording(context, time);
    }

    public RecordingService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        MwmApplication app = (MwmApplication) getApplication();
        if (!app.arePlatformAndCoreInitialized())
            app.initCore();
        handler = new Handler();
        receiver = new RecordingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(receiver, filter);
        startForeground(NOTIFICATION_RECORDING_ICON, build());
        refresh();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        if (intent != null) {
            String a = intent.getAction();
            if (a == null) {
                showNotificationAlarm(true);
            } else if (a.equals(STOP_BUTTON)) {
                stopService(this);
            } else if (a.equals(PAUSE_BUTTON)) {
                MwmActivity.pauseRecording(this);
            } else if (a.equals(SHOW_ACTIVITY)) {
                MwmActivity.startActivity(this);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public class Binder extends android.os.Binder {
        public RecordingService getService() {
            return RecordingService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestory");
        showNotificationAlarm(false);
        unregisterReceiver(receiver);
        handler.removeCallbacks(refresh);
    }

    Notification build() {
        PendingIntent main = PendingIntent.getService(this, 0,
                new Intent(this, RecordingService.class).setAction(SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pe1 = PendingIntent.getService(this, 0,
                new Intent(this, RecordingService.class).setAction(PAUSE_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pe2 = PendingIntent.getService(this, 0,
                new Intent(this, RecordingService.class).setAction(STOP_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        long time = shared.getLong(RECORDING, 0);

        NotificationChannelCompat channel = new NotificationChannelCompat(this, "recording", "Recording", NotificationManagerCompat.IMPORTANCE_LOW);

        RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(this, R.layout.notification);
        String title = getString(R.string.app_name);
        String text = SIMPLE_DATE_FORMAT.format(new Date(time)) + " - " + (TrackRecorder.isEnabled() ? "recording" : "paused");
        builder.setImageViewResource(R.id.notification_pause, TrackRecorder.isEnabled() ? R.drawable.ic_pause_black_48dp : R.drawable.ic_fiber_manual_record_red_48dp);
        builder.setOnClickPendingIntent(R.id.notification_pause, pe1);
        builder.setOnClickPendingIntent(R.id.notification_stop, pe2);

        builder.setTitle(title)
                .setText(text)
                .setMainIntent(main)
                .setTheme(ThemeUtils.isNightTheme() ? R.style.MwmTheme_Night : R.style.MwmTheme)
                .setChannel(channel)
                .setWhen(notification)
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_recording);

        if (!TrackRecorder.isEnabled())
            builder.setImageViewTint(R.id.notification_pause, Color.RED);

        return builder.build();
    }

    public void showNotificationAlarm(boolean show) {
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        if (!show) {
            nm.cancel(NOTIFICATION_RECORDING_ICON);
        } else {
            notification = build();
            nm.notify(NOTIFICATION_RECORDING_ICON, notification);
        }
    }

    void refresh() {
        handler.removeCallbacks(refresh);
        showNotificationAlarm(true);
        handler.postDelayed(refresh, 10 * 1000);
    }
}
